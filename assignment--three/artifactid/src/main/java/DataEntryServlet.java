import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

@WebServlet("/dataEntryServlet")
public class DataEntryServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //out.println("<h3>Hello World!</h3>");
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");
        String year = request.getParameter("year");
        String price = request.getParameter("price");

        System.out.println(title + "   " + year + "  " + price);

        String message = title+ "," + year + "," + price;
        DispatchService dispatchService = new DispatchService();
        dispatchService.sendMessage(message);

    }
}