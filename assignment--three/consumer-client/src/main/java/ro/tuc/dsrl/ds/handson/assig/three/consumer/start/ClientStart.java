package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;


import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {
	private final static String QUEUE_NAME = "hello";
	private ClientStart() {
	}

	public static void main(String[] args) throws TimeoutException {
		//QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		//MailService mailService = new MailService("your_account_here","your_password_here");
		//String message;

		//while(true) {
			try {
				//message = queue.readMessage();

				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost("localhost");
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();

				channel.queueDeclare(QUEUE_NAME, false, false, false, null);
				System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

				DeliverCallback deliverCallback = (consumerTag, delivery) -> {
					String message = new String(delivery.getBody(), "UTF-8");
					System.out.println(" [x] Received '" + message + "'");
					String[] parts = message.split(",");

                    PrintWriter writer = new PrintWriter(parts[0]+".txt", "UTF-8");
                    writer.println(message);
                    writer.close();
                    MailService mailService = new MailService("and.oprean1@gmail.com", "Nutella1");
                    mailService.sendMail("and.oprean1@gmail.com", "DVD Nou", message);
				};
				//System.out.println("Sending mail "+message);
				//mailService.sendMail("to_mail_address","Dummy Mail Title",message);
				channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
			} catch (IOException e) {
				e.printStackTrace();
			}
		//}
	}
}
